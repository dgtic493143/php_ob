<?php
//comando de inclusion con la ruta de las clases
include_once('../Clases/ejercicio1/Carro.php');
include_once('../Clases/ejercicio1/Moto.php');
?>


<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <title>Indice</title>
</head>
<body>
    
    <div class="container" style="margin-top: 4em">
        <header><h1>Carro y Moto</h1></header><br>

        <!-- Mostrar mensaje del Carro -->
        <input type="text" class="form-control" value="<?php echo $mensajeServidor; ?>" readonly>

        <!-- Formulario para el Carro -->
        <form method="post">
            <div class="form-group row">
                <label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="color" name="color" id="CajaTexto1">
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Enviar Carro</button>
        </form>

        <br>

        <!-- Mostrar mensaje de la Moto -->
        <input type="text" class="form-control" value="<?php echo $mensajeMoto; ?>" readonly>

        <!-- Formulario para la Moto -->
        <form method="post">
            <div class="form-group row">
                <label class="col-sm-3" for="CajaTexto2">Tamaño de la moto:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="tamanio" id="CajaTexto2" required>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-3" for="CajaTexto3">Tipo de la moto:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="tipo" id="CajaTexto3" required>
                </div>
            </div>

            <button class="btn btn-primary" type="submit">Enviar Moto</button>
        </form>

        <br>

        <a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
    </div>

</body>
</html>