<?php
// Comando de inclusión con la ruta de las clases
include_once('../Clases/ejercicio2/Carro2.php');

// Procesar el formulario si se ha enviado
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $color = $_POST['color'];
    $modelo = $_POST['modelo'];
    $anioFabricacion = $_POST['anioFabricacion'];  // Asegúrate de que el formulario tenga un campo para el año de fabricación

    // Crear instancia de la clase Carro2
    $Carro1 = new Carro2();
    $Carro1->color = $color;
    $Carro1->modelo = $modelo;
    $Carro1->verificacion($anioFabricacion);
}
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <title>Ejercicio 2</title>
</head>
<body>
    <div class="container" style="margin-top: 4em">
        <header><h1>La verificación</h1></header><br>
        <form method="post">
            <div class="form-group row">
                <label class="col-sm-1" for="CajaTexto1">Color:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="color" name="color" id="CajaTexto1">
                </div>
                <label class="col-sm-2 pl-md-5" for="CajaTexto2">Modelo:</label>
                <div class="col-sm-4 pl-lg-1">
                    <input class="form-control" type="text" name="modelo" id="CajaTexto2">
                </div>
                <label class="col-sm-1" for="CajaTexto3">Año de Fabricación:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="month" name="anioFabricacion" id="CajaTexto3" required>
                </div>            
            </div>
            <button class="btn btn-primary" type="submit">Enviar</button>
            <a class="btn btn-link offset-md-9 offset-lg-9 offset-7" href="../index.php">Regresar</a>
        </form>
    </div>

    <div class="container mt-5">
        <h1>Respuesta del servidor</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>Características de carro</th>
                    <th>Resultado de Verificación</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // Mostrar resultados si existe una instancia de Carro2
                if (isset($Carro1)) {
                    echo '<tr><td>Color:</td><td>' . $Carro1->getColor() . '</td></tr>';
                    echo '<tr><td>Modelo:</td><td>' . $Carro1->getModelo() . '</td></tr>';
                    echo '<tr><td>Resultado de Verificación:</td><td>' . $Carro1->getResultadoVerificacion() . '</td></tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</body>
</html>
