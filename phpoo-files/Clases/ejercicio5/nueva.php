<?php

class nueva{
    private $contrasena;

    // Constructor que genera una contraseña de 4 letras mayúsculas al instanciar la clase
    public function __construct()
    {
        $this->generarContrasena();
    }

    // Método para generar una contraseña de 4 letras mayúsculas
    private function generarContrasena()
    {
        $caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $longitud = 4;
        $this->contrasena = substr(str_shuffle($caracteres), 0, $longitud);
    }

    // Método mágico para mostrar la contraseña al destruir el objeto
    public function __destruct()
    {
        echo 'Contraseña destruida: ' . $this->contrasena;
    }
}

?>
