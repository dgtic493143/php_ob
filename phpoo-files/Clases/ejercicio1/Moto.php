<?php

class Moto {
    // Declaración de propiedades
    public $tamanio;
    public $tipo;
}

// Inicializamos el mensaje que lanzará el servidor con vacío para la Moto
$mensajeMoto = '';

// Verifica si se ha enviado una petición POST para la Moto
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Se instancia la clase Moto
    $Moto1 = new Moto;

    if (!empty($_POST['tamanio']) && !empty($_POST['tipo'])) {
        // Recibe los valores mandados por POST y los asigna a los atributos de la Moto
        $Moto1->tamanio = $_POST['tamanio'];
        $Moto1->tipo = $_POST['tipo'];
        // Se construye el mensaje que será lanzado por el servidor
        $mensajeMoto = 'El servidor dice que ya escogiste un tamaño para la moto: ' . $_POST['tamanio'] . ' y un tipo: ' . $_POST['tipo'];
    }
}

?>
