<?php

class Carro {
    // Declaración de propiedades
    public $color;
}

// Inicializamos el mensaje que lanzará el servidor con vacío
$mensajeServidor = '';

// Verifica si se ha enviado una petición POST para el Carro
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Se instancia la clase Carro
    $Carro1 = new Carro;

    if (!empty($_POST['color'])) {
        // Almacenamos el valor mandado por POST en el atributo color
        $Carro1->color = $_POST['color'];
        // Se construye el mensaje que será lanzado por el servidor
        $mensajeServidor = 'El servidor dice que ya escogiste un color para el carro: ' . $_POST['color'];
    }
}

?>
