<?php
class Carro2 {
    // Declaración de propiedades
    public $color;
    public $modelo;
    private $resultadoVerificacion;

    // Método para verificar el estado del carro según el año de fabricación
    public function verificacion($anioFabricacion) {
        if ($anioFabricacion < 1990) {
            $this->resultadoVerificacion = 'No';
        } elseif ($anioFabricacion >= 1990 && $anioFabricacion <= 2010) {
            $this->resultadoVerificacion = 'Revisión';
        } else {
            $this->resultadoVerificacion = 'Si';
        }
    }
	
    // Métodos para obtener propiedades
    public function getColor() {
        return $this->color;
    }

    public function getModelo() {
        return $this->modelo;
    }

    public function getResultadoVerificacion() {
        return $this->resultadoVerificacion;
    }
}
?>
