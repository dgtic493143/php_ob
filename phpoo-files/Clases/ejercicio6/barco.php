<?php

include_once('transporte.php');

class barco extends transporte{
    private $calado;

    public function __construct($nom, $vel, $com, $cal)
    {
        parent::__construct($nom, $vel, $com);
        $this->calado = $cal;
    }

    public function resumenBarco()
    {
        $mensaje = parent::crear_ficha();
        $mensaje .= '<tr>
                        <td>Calado:</td>
                        <td>' . $this->calado . '</td>                
                    </tr>';
        return $mensaje;
    }
}
?>
