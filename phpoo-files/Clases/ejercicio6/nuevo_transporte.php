<?php

include_once('transporte.php');

class nuevo_transporte extends transporte{
    private $nueva_propiedad;

    public function __construct($nom, $vel, $com, $nueva_prop)
    {
        parent::__construct($nom, $vel, $com);
        $this->nueva_propiedad = $nueva_prop;
    }

    public function resumenNuevoTransporte()
    {
        $mensaje = parent::crear_ficha();
        $mensaje .= '<tr>
                        <td>Nueva propiedad:</td>
                        <td>' . $this->nueva_propiedad . '</td>                
                    </tr>';
        return $mensaje;
    }
}
?>
