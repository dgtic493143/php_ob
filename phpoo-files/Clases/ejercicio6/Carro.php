<?php

include_once('transporte.php');

class carro extends transporte{
    private $numero_puertas;

    public function __construct($nom, $vel, $com, $pue)
    {
        parent::__construct($nom, $vel, $com);
        $this->numero_puertas = $pue;
    }

    public function resumenCarro()
    {
        $mensaje = parent::crear_ficha();
        $mensaje .= '<tr>
                        <td>Numero de puertas:</td>
                        <td>' . $this->numero_puertas . '</td>                
                    </tr>';
        return $mensaje;
    }
}
?>
